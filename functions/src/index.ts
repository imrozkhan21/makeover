import * as functions from 'firebase-functions';
import * as sendgrid from 'sendgrid';
const cors = require('cors')({origin: true});


// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const client = sendgrid("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");

function parseBody(body) {
 let helper = sendgrid.mail;
 let fromEmail = new helper.Email(body.from);
 let toEmail = new helper.Email(body.to);
 let subject = body.subject;
 let content = new helper.Content('text/html', body.content);
 let mail = new helper.Mail(fromEmail, subject, toEmail, content);
 return mail.toJSON();
}

exports.httpEmail = functions.https.onRequest((req, res) =>{
  cors(req, res, () => {
    return Promise.resolve()
      .then(() => {
        if (req.method !== 'POST') {
          const error = new Error('Only Post requests are accepted');
          error.message = '405';
          throw error;
        }

        const request = client.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: parseBody(req.body)
        });
        return client.API(request)
      }).then((response) => {
        if (response.body) {
          res.send(response.body);
        } else {
          res.end();
        }
      }).catch((err) => {
        console.error(err);
        return Promise.reject(err);
      });
  })
});
