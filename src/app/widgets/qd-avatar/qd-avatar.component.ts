import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'qd-avatar',
  templateUrl: './qd-avatar.component.html'
})

export class QDAvatarComponent {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) { }
}
