import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material.module';
import { QDAccountComponent } from './qd-account/qd-account.component';
import { QDAvatarComponent } from './qd-avatar/qd-avatar.component';
import { QDOverflowMenuComponent } from './qd-overflow-menu/qd-overflow-menu.component';

@NgModule({
  declarations: [
    QDAccountComponent,
    QDAvatarComponent,
    QDOverflowMenuComponent
  ],
  entryComponents: [],
  exports: [
    QDAccountComponent,
    QDAvatarComponent,
    QDOverflowMenuComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  providers: []
})

export class WidgetsModule { }
