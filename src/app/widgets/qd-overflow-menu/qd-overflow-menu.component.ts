import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'qd-overflow-menu',
  templateUrl: './qd-overflow-menu.component.html'
})

export class QDOverflowMenuComponent {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder) { }
}
