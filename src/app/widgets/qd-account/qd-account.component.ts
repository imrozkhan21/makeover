import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'qd-account',
  templateUrl: './qd-account.component.html'
})
export class QDAccountComponent {
  formGroup: FormGroup;
  showNarrow: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

    sidenavItems = [
        /*{
            icon: 'icon-home',
            label: 'Home',
            route: '/home'
        },*/
        {
            icon: 'icon-patients',
            label: 'People',
            route: '/people'
        }, {
            icon: 'icon-results',
            label: 'Services',
            route: '/services'
        }, {
            icon: 'icon-orders',
            label: 'Gallery',
            route: '/gallery'
        }, {
            icon: 'icon-help',
            label: 'Contact',
            route: '/contact'
        }
    ];
}
