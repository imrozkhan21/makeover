import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CoreService } from '../core.service';

@Component({
  selector: 'qd-toolbar',
  templateUrl: './toolbar.component.html'
})
export class ToolbarComponent {
  formGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private coreService: CoreService) {
    this.formGroup = this.formBuilder.group({
      qdGlobalSearch: ['']
    });
  }

  toggleSidenav = () => this.coreService.toggleSidenav();
}
