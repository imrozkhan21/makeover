import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material.module';
import { ConfirmationDialog } from './confirmation-dialog/confirmation-dialog.component';
import { CoreService } from './core.service';
import {SalonservicesService} from "./salonservices.service";

import { WidgetsModule } from '../widgets/widgets.module';
import { MainIndexComponent } from './main-index/main-index.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {SalonSingleData} from "./salon-single-data";

@NgModule({
  declarations: [
    ConfirmationDialog,
    MainIndexComponent,
    SidenavComponent,
    ToolbarComponent],
  entryComponents: [
    ConfirmationDialog
  ],
  exports: [
    ConfirmationDialog,
    MainIndexComponent,
    MaterialModule,
    SidenavComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
    WidgetsModule,
      FormsModule,
      MatDatepickerModule
  ],
  providers: [
    CoreService,
      SalonservicesService,
      SalonSingleData
  ]
})

export class CoreModule { }
