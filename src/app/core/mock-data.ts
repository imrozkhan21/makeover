import {SalonSingleData} from "./salon-single-data";

export const SalonMockData : SalonSingleData[] = [
        {"description" : 'A regular haircut is a men\'s and boys\' hairstyle that has hair long enough to comb on top, a defined or deconstructed side part, and a short, semi-short, medium, long, or extra long back and sides. The style is also known by other names including taper cut, regular taper cut, side-part and standard haircut; as well as short back and sides, business-man cut and professional cut, subject to varying national, regional, and local interpretations of the specific taper for the back and sides', "image" : "./assets/images/Services/men.png", "title": "Men Haircut"},
        {
            "description" : "Ready to have the BEST hair of your life? We have found the perfect style for you. Forget about hair trends! Forget about trimming up your five-year-old bob. It’s time to find your signature style.If you don’t know what you’re doing, long hair can look drab. It’s all too easy to live in a top knot 24/7 when your locks turn lame. Want a wow-factor? We’ve got you covered.Mastering the perfect hairstyle is all about rocking your unique style. If you want to get your dream hair, you need to make your look work for you! The key to it all is figuring out your face shape. Once you know your shape, you can find an ultra-flattering ‘do that works for YOU!", "image" : "./assets/images/Services/women.png", "title": "Women Haircut"
        },
        {
            "description" : 'Cosmetics are substances or products used to enhance or alter the appearance or fragrance of the body. Many cosmetics are designed for use of applying to the face and hair. They are generally mixtures of chemical compounds; some being derived from natural sources (such as coconut oil), and some being synthetics. Common cosmetics include lipstick, mascara, eye shadow, foundation, rouge, skin cleansers and skin lotions, shampoo, hairstyling products (gel, hair spray, etc.), perfume and cologne. Cosmetics applied to the face to enhance its appearance are often called make-up or makeup.', "image" : "./assets/images/Services/makeup.png", "title": "Make Up"
        },
        {
            "description" : 'Rebonding is a special technique that straightens hair for those who wish to have straight hair as opposed to wavy or curly hair. Smoothing is a procedure that is designed to make hair softer and smoother so as to make it more silky and manageable.', "image" : "./assets/images/Services/rebonding.png", "title": "ReBonding/Smoothning"
        },
    ];