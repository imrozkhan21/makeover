import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'qd-dialog-confirmation',
  templateUrl: './confirmation-dialog.component.html'
})
export class ConfirmationDialog {
  public message: string;

  constructor(public dialogRef: MatDialogRef<ConfirmationDialog>) { }
}
