import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { map, shareReplay, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import {
  MatDialog,
  MatDialogRef,
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarRef,
  SimpleSnackBar
} from '@angular/material';
import { ConfirmationDialog } from './confirmation-dialog/confirmation-dialog.component';

const MAX_WIDTH = 1279;
const MAX_WIDTH_MATCH = `(max-width: ${MAX_WIDTH}px)`;

export interface ISnackbarPayload {
  message: string;
  action?: string;
  config?: MatSnackBarConfig;
}

@Injectable()
export class CoreService {
  isSmallScreen$: Observable<boolean>;
  showSidenavSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private dialog: MatDialog,
    private media: ObservableMedia,
    private snackBar: MatSnackBar
  ) {
    this.isSmallScreen$ = this.media.asObservable()
      .pipe(map(() => this.media.isActive(MAX_WIDTH_MATCH)), shareReplay());
  }

  closeSidenav = () => {
    console.log("close", this.showSidenavSubject)
    return this.showSidenavSubject.next(false)};

  openSidenav = () => {
    console.log("open")
    return this.showSidenavSubject.next(true)};

  toggleSidenav = () => {
      console.log("toggle", this.showSidenavSubject.value)
    return this.showSidenavSubject.value ? this.closeSidenav() : this.openSidenav()};

  async confirm(title: string, message: string): Promise<boolean> {
    const dialogRef: MatDialogRef<ConfirmationDialog> = this.dialog.open(ConfirmationDialog, {
      panelClass: 'qd-dialog-confirmation'
    });
    dialogRef.componentInstance.message = message;

    return await dialogRef.afterClosed().toPromise();
  }

  openSnackbar = (payload: ISnackbarPayload): MatSnackBarRef<SimpleSnackBar> =>
    this.snackBar.open(payload.message, payload.action, payload.config)

  openDefaultSnackbar = (message: string): MatSnackBarRef<SimpleSnackBar> =>
    this.openSnackbar({ action: undefined, config: { duration: 2000 }, message })
}
