import { combineLatest } from 'rxjs/observable/combineLatest';
import { map, shareReplay } from 'rxjs/operators';

import { Component, Input, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { CoreService } from '../core.service';

@Component({
  selector: 'qd-sidenav',
  templateUrl: './sidenav.component.html'
})
export class SidenavComponent {
  showNarrow = false;
  showMore = false;
  mode$: Observable<string>;
  opened$: Observable<boolean>;

  constructor(private coreService: CoreService) {
    this.mode$ = this.coreService.isSmallScreen$.pipe(map(isSmallScreen => isSmallScreen ? 'over' : 'side'), shareReplay());

    this.opened$ = combineLatest(this.coreService.isSmallScreen$, this.coreService.showSidenavSubject.asObservable())
    .pipe(map(([isSmallScreen, showSideNav]) => !isSmallScreen || showSideNav), shareReplay());
  }

    sidenavItems = [
        /*{
            icon: 'icon-home',
            label: 'Home',
            route: '/home'
        },*/
        {
            icon: 'icon-patients',
            label: 'People',
            route: '/people'
        }, {
            icon: 'icon-results',
            label: 'Services',
            route: '/services'
        }, {
            icon: 'icon-orders',
            label: 'Gallery',
            route: '/gallery'
        }, {
            icon: 'icon-help',
            label: 'Contact',
            route: '/contact'
        }
    ];

  closeSidenav = () => this.coreService.closeSidenav();
}
