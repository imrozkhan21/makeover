import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import { map, shareReplay, tap } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import {SalonMockData} from "./mock-data";
import { ObservableMedia } from '@angular/flex-layout';

const MAX_WIDTH = 1279;
const MAX_WIDTH_MATCH = `(max-width: ${MAX_WIDTH}px)`;

@Injectable()
export class SalonservicesService {
    public fetchSalonData(): Observable<any> {

        return Observable.of(SalonMockData).map(o => {
          return o;
        });
    }

}
