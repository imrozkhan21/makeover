import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { CoreService } from '../core/core.service';
import {SalonSingleData} from "../core/salon-single-data";
import {SalonservicesService} from "../core/salonservices.service";
import { Subject } from 'rxjs/Subject';

@Component({
    templateUrl: './services.component.html',
})
export class ServicesComponent implements OnInit {
    formGroup: FormGroup;
    salonServicesInfo;
    clickStream;
    activeIndex : number;


    constructor(private salonService: SalonservicesService, private formBuilder: FormBuilder) {

    }

    ngOnInit(): void {
        this.activeIndex = 0;
        this.confirm();
    }

    confirm = async () => {
            await this.salonService.fetchSalonData().subscribe(data => {
            this.salonServicesInfo = data;
            console.log("data", data)
        });
    };

    previous() : void {
        this.activeIndex > 0 ? this.activeIndex-- : this.activeIndex = (this.salonServicesInfo.length - 1);
    }

    next(): void {
        this.activeIndex == (this.salonServicesInfo.length - 1) ?  this.activeIndex = 0 :  this.activeIndex ++;
    }

}
