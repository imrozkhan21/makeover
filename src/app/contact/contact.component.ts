import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import {Http, Headers, Response, URLSearchParams} from "@angular/http";
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import {AngularFireDatabase  } from "angularfire2/database";


@Component({
    templateUrl: './contact.component.html',
})
export class ContactComponent implements OnInit {
    isLinear = true;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;

    constructor(private _formBuilder: FormBuilder, private http: HttpClient, private db: AngularFireDatabase) {

    }

    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', Validators.required],
            emailCtrl : ['', Validators.required],
            phoneCtrl : ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', Validators.required],
            cityCtrl: ['', Validators.required],
            stateCtrl : ['', Validators.required],
            zipCtrl : ['', Validators.required]
        });
    }

    saveUserInfo(emailAddress, content) {
        console.log("form", this.firstFormGroup, "2", this.secondFormGroup);
        let url = 'https://us-central1-makeover-284bd.cloudfunctions.net/httpEmail';
        let params : HttpParams = new HttpParams();
        let options ={
          headers: new HttpHeaders({
            'Content-Type' : 'application/json',
            'Access-Control-Allow-Origin': '*'
          })
        } ;

        params.set('to', emailAddress);
        params.set('from', 'imrozkhn7@gmail.com');
        params.set('content', content);
        params.set('subject', 'I am subject');

        return this.http.post(url, params, options)
          .toPromise()
          .then(res => {
            console.log(res)
          }).catch(err =>{
            console.log(err)
          })
    }

    sendSMS() {

    }


}
