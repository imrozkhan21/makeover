import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainIndexComponent } from './core/main-index/main-index.component';
import { HomeComponent } from './home/home.component';

export const routes: Routes = [
  {
    children: [{
      component: HomeComponent,
      path: ''
    }],
    component: MainIndexComponent,
    path: ''
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
