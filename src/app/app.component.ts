import { Component } from '@angular/core';

@Component({
  selector: 'qd-app-root',
  templateUrl: './app.component.html'
})
export class AppComponent { }
