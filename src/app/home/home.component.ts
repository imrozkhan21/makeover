import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CoreService } from '../core/core.service';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private coreService: CoreService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      address: ['', Validators.required],
      city: ['', Validators.required],
      email: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      state: ['', Validators.required],
      zip: ['', Validators.required]
    });
  }

  confirm = async () => {
    const confirmed = await this.coreService.confirm('Confirm', 'Do you confirm?');
    console.log(`Confirmed: ${confirmed}`);
  }

  openSnackBar = () => {
    this.coreService.openDefaultSnackbar('Default SnackBar');
  }
}
