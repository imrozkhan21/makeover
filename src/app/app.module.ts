import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home/home.component';
import { WidgetsModule } from './widgets/widgets.module';
import {MatNativeDateModule} from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import {PeopleComponent} from './people/people.component';
import {ServicesComponent} from "./services/services.component";
import {ContactComponent} from "./contact/contact.component";
import {MatStepperModule} from '@angular/material/stepper';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from "angularfire2/database";
import {AngularFireAuthModule} from "angularfire2/auth";
import {environment} from "../environments/environment";
import {MatToolbarModule} from '@angular/material/toolbar';
import { HttpClientModule, HttpClient } from '@angular/common/http';



const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: '',      component: HomeComponent },
    { path: 'people', component: PeopleComponent},
    { path: 'services', component: ServicesComponent},
    { path: 'contact', component: ContactComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
      PeopleComponent,
      ServicesComponent,
      ContactComponent
  ],
  exports: [
    AppComponent,
    ReactiveFormsModule
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    WidgetsModule,
      MatNativeDateModule,
      RouterModule.forRoot(
          appRoutes,
          { enableTracing: true } // <-- debugging purposes only
      ),
      MatStepperModule,
    MatToolbarModule,
      AngularFireModule.initializeApp(environment.firebase),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
    HttpClientModule
      // other imports here
  ],
  providers: [HttpClient, HttpClientModule],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
